# WordPress Website Environment Plugin Plugin

This WordPress plugin shows a notice in the admin bar for which website environment you are on and a new Admin colour scheme which you can define in wp-config. A typical workflow includes Local, Staging and Production and a typical enterprise-level workflow would also add QA between Staging and Production - this plugin allows for you to easily see what version you are on.

1. Install & Activate Plugin

2. In wp-config.php add one of the following:
- define( 'WEBSITE_ENVIRONMENT', 'local' );
- define( 'WEBSITE_ENVIRONMENT', 'staging' );
- define( 'WEBSITE_ENVIRONMENT', 'qa' );
- define( 'WEBSITE_ENVIRONMENT', 'production' );

3. If you want to change colour you can, in wp-config.php add one of the following (default values shown below):
- define( 'WEBSITE_ENVIRONMENT_LOCAL', 'coffee' );
- define( 'WEBSITE_ENVIRONMENT_STAGING', 'midnight' );
- define( 'WEBSITE_ENVIRONMENT_QA', 'default' );
- define( 'WEBSITE_ENVIRONMENT_PRODUCTION', 'default' );

Available by default: default, light, blue, coffee, ectoplasm, midnight, ocean, sunrise

4. By default if the siteurl ends in .test then it uses the local environment unless otherwise defined in wp-config.php.


## Hire me
Hey! I'm a [freelance WordPress developer and technical consultant](https://mostlycaffeine.com) with a mission to deliver accessible, performant, secure websites that are sustainable and respect human rights. I'm available to hire for full projects or contract work, and would love to work with you!
- https://mostlycaffeine.com