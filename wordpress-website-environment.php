<?php
/**
 * WordPress Website Environment Plugin
 *
 *
 * @link              https://mostlycaffeine.com
 * @since             1.0.0
 * @package           wpwebsiteenv
 *
 * @wordpress-plugin
 * Plugin Name:       WordPress Website Environment
 * Plugin URI:        https://mostlycaffeine.com/
 * Description:       This changes the Admun area based on the environment of the website.
 * Version:           1.0.0
 * Author:            @mostlycaffeine
 * Author URI:        https://mostlycaffeine.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wpwebsiteenv
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {

	die;

}


/**
 * Current plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 * No ! define because it shouldn't be overwritten by anything else.
 */
define( 'WPWEBSITEDENV_VERSION', '1.0.0' );


if ( ! defined( 'WPWEBSITEDENV_PLUGIN_DIR' ) ) {

	define( 'WPWEBSITEDENV_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

}


class MC_WPWebsiteEnv {

	public function __construct() {

		// If the URL domain is .test treat this as a websites local environment unless overwritten.
		if ( ! defined( 'WEBSITE_ENVIRONMENT' ) && is_admin() ) {

			$site_url     = esc_url( get_site_url() );
			$site_url     = explode( '.', wp_parse_url( $site_url, PHP_URL_HOST ) );
			$site_url_tld = end( $site_url );

			if ( 'test' === $site_url_tld ) {

				define( 'WEBSITE_ENVIRONMENT', 'local' );

			}
		}

		// If a website environment is defined then add the admin bar and set admin colour scheme.
		if ( defined( 'WEBSITE_ENVIRONMENT' ) ) {

			// Don't want to add the admin bar text if it's a Production website as could confuse the user.
			if ( 'local' === WEBSITE_ENVIRONMENT || 'staging' === WEBSITE_ENVIRONMENT || 'qa' === WEBSITE_ENVIRONMENT ) {

				add_action( 'admin_bar_menu', array( $this, 'mc_wpwebsiteenv_adminbar' ) );

				add_action( 'admin_head', array( $this, 'mc_wpwebsiteenv_adminbar_css' ) );

			}

			add_filter( 'get_user_option_admin_color', array( $this, 'mc_wpwebsiteenv_colours' ) );

		}

	}

	/**
	 * Display a notice for extra visibility (apart from on Production).
	 */
	public function mc_wpwebsiteenv_adminbar( $wp_admin_bar ) {

		$args = array(
			'id'    => 'my_page',
			'title' => WEBSITE_ENVIRONMENT . ' env.',
			'meta'  => array( 'class' => 'mc-wpwebsiteenv-menuitem' ),
		);

		$wp_admin_bar->add_node( $args );

	}

	/**
	 * Admin bar CSS to make it stand out a bit.
	 */
	public function mc_wpwebsiteenv_adminbar_css() {

		echo '<style>
		.mc-wpwebsiteenv-menuitem,
		.mc-wpwebsiteenv-menuitem div {
			font-weight: bold !important;
			text-transform: uppercase !important;
		}
		</style>';

	}

	/**
	 * Get the correct colour scheme either from wp-config or the defaults.
	 */
	public function mc_wpwebsiteenv_colours() {

		// Default admin colours.
		$admin_colours = array(
			'local'      => 'coffee',
			'staging'    => 'midnight',
			'qa'         => 'default',
			'production' => 'default',
		);

		// Check if a colour scheme has been defined.
		if ( defined( 'WEBSITE_ENVIRONMENT_LOCAL' ) ) {

			$local = WEBSITE_ENVIRONMENT_LOCAL;

		}
		if ( defined( 'WEBSITE_ENVIRONMENT_STAGING' ) ) {

			$staging = WEBSITE_ENVIRONMENT_STAGING;

		}
		if ( defined( 'WEBSITE_ENVIRONMENT_QA' ) ) {

			$qa = WEBSITE_ENVIRONMENT_QA;

		}
		if ( defined( 'WEBSITE_ENVIRONMENT_PRODUCTION' ) ) {

			$production = WEBSITE_ENVIRONMENT_PRODUCTION;

		}

		// Check if a specified encironment has a specified colour scheme.
		// Otherwise display the plugins set defaults.
		if ( 'local' === WEBSITE_ENVIRONMENT && ! empty( $local ) ) {

			return $local;

		} elseif ( 'staging' === WEBSITE_ENVIRONMENT && ! empty( $staging ) ) {

			return $staging;

		} elseif ( 'qa' === WEBSITE_ENVIRONMENT && ! empty( $qa ) ) {

			return $qa;

		} elseif ( 'production' === WEBSITE_ENVIRONMENT && ! empty( $production ) ) {

			return $production;

		} else {

			return $admin_colours[ WEBSITE_ENVIRONMENT ];

		}

	}

}

$mc_wpwebsiteenv = new MC_WPWebsiteEnv();
